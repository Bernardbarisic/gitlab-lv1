﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note = new Note();
            Note John = new Note("John", "Exercising", 15);
            Note Max = new Note("Max", "Work");

            Console.WriteLine(John.Importance);
            Console.WriteLine(note.AuthorNote);
            Console.WriteLine(Max.Importance);
            Console.WriteLine(Max.ToString());
            Console.WriteLine(John);

        }
    }
}
