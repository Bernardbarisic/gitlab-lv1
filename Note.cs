﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp
{
    class Note
    {
        private String textnote;
        private String authornote;
        private int importance;

        int getImportance() { return importance; }
        void setImportance(int value) { importance = value; }

        public int Importance
        {
            get { return importance; }
            set { this.importance = value; }
        }

        public String TextNote
        {
            get { return textnote; }
            set { this.textnote = value; }
        }

        public String AuthorNote
        {
            get { return authornote; }
        }

        public Note()
        {
            this.textnote = "XXX";
            this.authornote = "Title";
            this.Importance = 0;
        }
        public Note(String TextNote, String AuthorNote, int Importance)
        {
            this.textnote = TextNote;
            this.authornote = AuthorNote;
            this.importance = Importance;
        }
        public Note(String TextNote, String AuthorNote)
        {
            this.textnote = TextNote;
            this.authornote = AuthorNote;
        }

        public override string ToString()
        {
            return (this.authornote + this.importance);
        }
    
    }
}
